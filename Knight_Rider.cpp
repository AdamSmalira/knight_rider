/* 
prezentacja krótka       https://www.youtube.com/watch?v=v8X9Tsic0Go&feature=youtu.be
wersja z omówieniem kodu https://www.youtube.com/watch?v=WWKMzQ4JQKM&feature=youtu.be
uruchomienie:
clear && sudo g++ -lwiringPi -lpthread AdamSmalira_intro.cpp && ./a.out

Program uruchamia szereg 8. ledów na wzór z Knight Rider, przy użyciu sygnału PWM i 4. wątków.
Są trzy przerwania sygnału:
-krótkie wciśnięcie guzika direction: zmiana kierunku wyświetlania ledów
-krótkie wciśnięcie guzika speed: przyspieszenie/spowolnienie zapalania ledów
-długie wciśnięcie guzika speed: wyjście z programu

Autor: Adam Smalira
*/

#include <iostream>
#include <wiringPi.h>
#include <softPwm.h>
#include <vector>
#include <thread>

using namespace std;

void light_up(const int* pin, const int& time);
void light_down(const int* pin, const int& time);
int turn_right(const vector<const int*>& vec, const int& speed, thread& thread_stop_right, int start_vec, bool& chng_direction, const bool& stop_program);
int turn_left(const vector<const int*>& vec, const int& speed, thread& thread_stop_left, int end_vec, bool& chng_direction, const bool& stop_program);

void My_Thread_Stop_Right(const vector<const int*>& obj, const int& speed, const bool& kill_right);
void My_Thread_Stop_Left(const vector<const int*>& obj, const int& speed, const bool& kill_left, const int& end_vec);
void My_Thread_Buttons(bool& chng_direction, const int& direction_pin, int& speed, const int& speed_pin, bool& stop_program);

int main(void)
{
    const int a1 = 5, a2 = 11, a3 = 9, a4 = 10, a5 = 22, a6 = 15, a7 = 18, a8 = 23;
    const int direction_pin = 14;
    const int speed_pin = 26;
    vector<const int*> vec {&a1, &a2, &a3, &a4, &a5, &a6, &a7, &a8};

    if(wiringPiSetupGpio() != 0)
    {
        cout << "Fail GPIO Initialization" << endl;
        return EXIT_FAILURE;
    }

    for(const auto& i: vec)
        softPwmCreate(*i, 0, 100); //seting GPIOs for LED software pwm

    /* interrupt PINs requires +3,3V and 10kOhm resistor, 
    I use RPI4 and pinMode(gpio_nr, PULLUP) doesn't work on RPI4 && wirignPI) */ 
    pinMode(speed_pin, INPUT); //change speed buton and stop
    pinMode(direction_pin, INPUT); //change direction of LEDs

 
    thread thread_stop_right;
    thread thread_stop_left; 
    thread thread_buttons;
    int speed = 5;
    bool chng_direction = false;
    bool stop_program = false;

    int start_vec = 0;
    int end_vec = 7;
    int possition_left = start_vec;
    int possition_right;

    /* runing thread to detect signal interrupts on buttons in endless loop */
    thread_buttons = thread(My_Thread_Buttons,std::ref(chng_direction), std::ref(direction_pin), std::ref(speed), std::ref(speed_pin), std::ref(stop_program));
    try{
    while(1)
    {
        possition_right = turn_right(vec, speed, thread_stop_right, possition_left, chng_direction, stop_program);
        if(possition_right == 0)
        {
            possition_right = end_vec;
        }
        possition_left = turn_left(vec, speed, thread_stop_left, possition_right, chng_direction, stop_program);
        if(possition_left == 0)
        {
            possition_left = start_vec;
        }
        possition_right = end_vec;
        
        if(stop_program)
        {
            break;
        }
    }
    thread_buttons.join();
    }
    catch(const exception& er)
    {
        cout << "Exiting program" << endl;
        
        /* cleaning */
        if(thread_buttons.joinable())
            thread_buttons.join();
        if(thread_stop_right.joinable())
            thread_stop_right.join();
        if(thread_stop_left.joinable())  
            thread_stop_left.join();

        for(const auto& i: vec)
            pinMode(*i, INPUT);

        return EXIT_SUCCESS;
    }
    return EXIT_SUCCESS;
}
void My_Thread_Buttons(bool& chng_direction, const int& direction_pin, int& speed, const int& speed_pin, bool& stop_program)
{
    int counter = 0;
    int speed_slow = 5;
    int speed_fast = 1;
    while (true)
    {
        /* Change direction statetment BUTTON NR1 */
        if(digitalRead(direction_pin) == 0 && chng_direction == 0) 
        {
            chng_direction = 1;
            delay(500);
            cout << "Changing direction" << endl;
        }
        if(digitalRead(direction_pin) == 1 && chng_direction == 1)
        {
            chng_direction = 0;
        }

        /* Change speed or exit the program statement BUTTON NR2 */
        if(digitalRead(speed_pin) == 0)
        {
            while(true)
            {
                delay(250);
                counter++;
                cout << counter << endl;   
                if(digitalRead(speed_pin) == 1)
                {
                    break;
                }
                if (counter > 3) // if button pressed for 1s -> exit
                {
                    stop_program = true;
                    return;
                }
            }
            if(counter) // if button pressed and program didnt exit, chage speed slow/fast
            {
                if(speed == speed_fast)
                {
                    speed = speed_slow;
                }
                else
                {
                    speed = speed_fast;
                }
            }
            counter = 0;
        }
    }
}

int turn_right(const vector<const int*>& vec, const int& speed, thread& thread_stop_right, int start_vec, bool& chng_direction, const bool& stop_program)
{
    bool kill_right = false;
    for (int j = start_vec + 1; j < vec.size(); j++)
    {
        light_up(vec[j], speed);
        if (j == start_vec + 1)
        {
            thread_stop_right = thread(My_Thread_Stop_Right, std::ref(vec), std::ref(speed), std::ref(kill_right));
        }
        if(chng_direction == true)
        {
            chng_direction = false;
            kill_right = true;
            thread_stop_right.join();
            return j;
        }
        if(stop_program == true)
        {//sudo alsa force-reload
            kill_right = true;
            thread_stop_right.join();
            throw exception();
        }
    }
    thread_stop_right.join();
    
    return 0;
}
int turn_left(const vector<const int*>& vec, const int& speed, thread& thread_stop_left, int end_vec, bool& chng_direction, const bool& stop_program)
{
    bool kill_left = false;
    for (int j = end_vec - 1; j >= 0; j--)
    {
        light_up(vec[j], speed);
        if (j == end_vec - 1)
        {
            thread_stop_left = thread(My_Thread_Stop_Left, std::ref(vec), std::ref(speed), std::ref(kill_left), std::ref(end_vec));
        }
        if(chng_direction == true)
        {
            chng_direction = false;
            kill_left = true;
            thread_stop_left.join();
            return j;
        }
        if(stop_program == true)
        {
            kill_left = true;
            thread_stop_left.join();
            throw exception();
        }

    }
    thread_stop_left.join();

    return 0;
}

void My_Thread_Stop_Right(const vector<const int*>& obj, const int& speed, const bool& kill_right)
{
    for(size_t i = 0; i < obj.size()-1; i++ )
        {
            if(kill_right)
                return;

            if(digitalRead(*obj[i]) == 1)
            {
                light_down(obj[i], speed);
            }
        }
    return;
}

void My_Thread_Stop_Left(const vector<const int*>& obj, const int& speed, const bool& kill_left, const int& end_vec)
{  
    for(int i = end_vec - 0; i >= 0 + 1; i-- )
        {
            if(kill_left)
                return;

            light_down(obj[i], speed);
        }
    return;
}

void light_up(const int* pin, const int& time)
{
    int value = 0;
    while(value != 100)
    {
       ++value;
       softPwmWrite (*pin, value);
       delay(time);
    }
}
void light_down(const int* pin, const int& time)
{
    int value = 100;
    while(value != 0)
    {
       --value;
       softPwmWrite (*pin, value);
       delay(time);
    }
}
